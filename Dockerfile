# syntax=docker/dockerfile:1
FROM python:3.10-alpine3.15 as builder

WORKDIR /app

RUN apk add --no-cache --virtual .build-deps \
        gcc \
        libc-dev \
        libffi-dev \
        openssl-dev \
        make \
    && pip install --no-cache-dir poetry==1.1.13

COPY pyproject.toml poetry.lock ./

RUN poetry export -f requirements.txt > requirements.txt \
    && pip install --no-cache-dir -r requirements.txt

COPY . .

RUN poetry build && \
    tar -xf dist/*.tar.gz -C / && \
    rm -rf dist/* && \
    mv /flask-sample-app-* /app

FROM python:3.10-alpine3.15

WORKDIR /app

COPY --from=builder /app /app

RUN addgroup -g 1000 appuser && \
    adduser -D -u 1000 -G appuser appuser && \
    chown -R appuser:appuser /app

USER appuser

ENTRYPOINT [ "flask" ]

CMD ["run", "--host", "0.0.0.0", "--port", "8080"]